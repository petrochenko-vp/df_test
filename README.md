# Поиск бага, из-за которого в GitLab не отображаются датафреймы в тетрадках ipynb, созданных в PyCharm

### Список файлов:
- `df_jupyter.ipynb` - тетрадка с примером датафрейма, созданная/сохранённая в Jupyter
- `df_pycharm.ipynb` - тетрадка с тем же примером датафрейма, созданная/сохранённая в PyCharm. BUG: При просмотре тетрадки в GitLab не отображаются датафреймы
- `df_pycharm_fixed.ipynb` - исправленная тетрадка из PyCharm с тем же примером датафрейма. Датафрейм теперь отображается

В Jupyter html-код таблицы датафрейма записывается в виде списка строк, вот так:

```
"text/html": [
    "<div>\n",
    "<style scoped>\n",
    ...
    "</table>\n",
    "</div>"
]
```

В PyCharm'е этот же код таблицы записывается одной строкой, вот так:

```
"text/html": "<div>\n<style scoped>\n ... </table>\n</div>"
```

GitLab ничего не имеет против записи одной строкой, **НО её тоже нужно обернуть в список**, тогда датафрейм будет отображаться:

```
"text/html": ["<div>\n<style scoped>\n ... </table>\n</div>"]
```
